import React, { Component } from 'react';
import { StyleSheet, View, Image, Text, TouchableOpacity } from 'react-native';
import { Form, Item, Input, Label } from 'native-base';
import axios from 'axios';
import qs from 'qs';
import { connect } from 'react-redux';

class Login extends Component {
  static navigationOptions = { headerShown: false };
  state = {
    email: '',
    password: ''
  };

  handleInput = (text, type) => {
    this.setState({ [type]: text });
  };

  handleLogin = () => {
    const { email, password } = this.state;
    const body = {
      email,
      password
    };

    axios
      .post('http://20.20.20.237:3001/auth/login', qs.stringify(body))
      .then(response => {
        if (response.status === 200) {
          this.props.setDataLogin(response.data.data);
          this.props.navigation.navigate('App');
        } else {
          alert('Error cuy');
        }
      })
      .catch(() => {
        // alert('error');
      });
  };

  render() {
    const { email, password } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.card}>
          <Form>
            <Image
              source={require('../../Public/Assets/Image/logo.png')}
              style={styles.imgLogo}
            />
            <Item floatingLabel>
              <Label>Email</Label>
              <Input
                textContentType="emailAddress"
                onChangeText={text => this.handleInput(text, 'email')}
                value={email}
              />
            </Item>
            <Item floatingLabel>
              <Label>Password</Label>
              <Input
                secureTextEntry
                onChangeText={text => this.handleInput(text, 'password')}
                value={password}
              />
            </Item>
          </Form>
          <TouchableOpacity onPress={this.handleLogin}>
            <View style={styles.button}>
              <Text style={styles.textLogin}>Login</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};

const mapDispatchToProps = dispatch => ({
  setDataLogin: payload =>
    dispatch({
      type: 'POST_LOGIN_FULFILLED',
      payload
    })
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);

const styles = StyleSheet.create({
  container: {
    padding: 16,
    justifyContent: 'center',
    flex: 1,
    backgroundColor: '#f9f9f9'
  },
  card: {
    backgroundColor: 'white',
    elevation: 15,
    padding: 16,
    borderRadius: 8
  },
  imgLogo: {
    height: 150,
    width: 150,
    alignSelf: 'center'
  },
  button: {
    backgroundColor: 'brown',
    padding: 16,
    alignItems: 'center',
    borderRadius: 8,
    marginTop: 32
  },
  textLogin: {
    fontSize: 14,
    fontWeight: 'bold',
    color: 'white'
  }
});
