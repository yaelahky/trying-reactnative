import React, { Component } from 'react';
import { View, FlatList, Text, Image } from 'react-native';

const datasss = [
  {
    id: 1,
    name: 'Kopi',
    price: 10000,
    image:
      'https://doktersehat.com/wp-content/uploads/2018/11/kopi-doktersehat.jpg'
  },
  {
    id: 2,
    name: 'Pikopi',
    price: 10000,
    image:
      'https://doktersehat.com/wp-content/uploads/2018/11/kopi-doktersehat.jpg'
  },
  {
    id: 3,
    name: 'Ini kopi',
    price: 10000,
    image:
      'https://doktersehat.com/wp-content/uploads/2018/11/kopi-doktersehat.jpg'
  },
  {
    id: 4,
    name: 'Memang kopi',
    price: 10000,
    image:
      'https://doktersehat.com/wp-content/uploads/2018/11/kopi-doktersehat.jpg'
  },
  {
    id: 5,
    name: 'Ya kopi',
    price: 10000,
    image:
      'https://doktersehat.com/wp-content/uploads/2018/11/kopi-doktersehat.jpg'
  },
  {
    id: 5,
    name: 'Ya kopi',
    price: 10000,
    image:
      'https://doktersehat.com/wp-content/uploads/2018/11/kopi-doktersehat.jpg'
  },
  {
    id: 5,
    name: 'Ya kopi',
    price: 10000,
    image:
      'https://doktersehat.com/wp-content/uploads/2018/11/kopi-doktersehat.jpg'
  },
  {
    id: 5,
    name: 'Ya kopi',
    price: 10000,
    image:
      'https://doktersehat.com/wp-content/uploads/2018/11/kopi-doktersehat.jpg'
  },
  {
    id: 5,
    name: 'Ya kopi',
    price: 10000,
    image:
      'https://doktersehat.com/wp-content/uploads/2018/11/kopi-doktersehat.jpg'
  }
];

class Home extends Component {
  render() {
    return (
      <View>
        <Text>Selamat datang di toko kami</Text>
        <FlatList
          data={datasss}
          keyExtractor={item => item.id}
          renderItem={({ item, index }) => {
            //disini letak fungsi sederhana
            return (
              <View
                style={{
                  backgroundColor: 'white',
                  elevation: 15,
                  margin: 16,
                  padding: 16,
                  borderRadius: 8,
                  marginBottom: 0,
                  flexDirection: 'row'
                }}>
                <Image
                  style={{ height: 50, width: 50 }}
                  source={{
                    uri:
                      'https://doktersehat.com/wp-content/uploads/2018/11/kopi-doktersehat.jpg'
                  }}
                />
                <View style={{ marginLeft: 8 }}>
                  <Text>{item.name}</Text>
                  <Text>Harganya: {item.price}</Text>
                </View>
              </View>
            );
          }}
        />
      </View>
    );
  }
}

export default Home;
